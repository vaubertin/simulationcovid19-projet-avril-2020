# --- BIBLIOTHEQUES UTILISEES ---#
import numpy as np
import matplotlib.pyplot as plt
from typing import List, Tuple, Dict

d = 1 # Figure entre -d et d en abscisses et ordonnées
fig, ax = plt.subplots()
plt.axis([-d, d, -d, d])  
# Valeurs de départ de la position (premières valeurs de la liste : x et y) , de la vitesse (dernières valeurs de la liste : vx et vy) et de la couleur initial (valeurs du dictionnaire) de la balle
couleur:Dict[int,str]={0:"green",1:"green",2:"red",3:"green",4:"green",5:"green",6:"green"}
données:List[List[int]] = [[0.48,-0.17,0.039,-0.017],[0.23,-0.56,-0.039,0.017],[-0.15,-0.56,-0.039,-0.017],[0.98,0.45,-0.039,0.017],[0.65,-0.12,-0.039,-0.017],[-0.32,0.96,0.039,0.017],[0.79,-0.12,0.039,-0.017]]
# Rayon de la balle : 
rayon = d/50
rbd = 0 # Nbr de rebonds avant l'arrêt

# --- CODE PRINCIPAL --- #
while rbd <50:
    for élément in données:
        cercle_blanc = plt.Circle((élément[0], élément[1]), radius=rayon, fill = True, facecolor="white", edgecolor="white")
        ax.add_artist(cercle_blanc)
        # --- GESTION DES REBONDS SUR LES BORDS ---#
        if abs(élément[0] + élément[2]) > d - rayon: 
            élément[2] = -élément[2]
            rbd += 1
        if abs(élément[1] + élément[3]) > d - rayon:
            élément[3] = -élément[3]
            rbd += 1
        # Avance 
        élément[0] += élément[2]
        élément[1] += élément[3]
        # On dessine la balle 
        cercle = plt.Circle((élément[0], élément[1]), radius = rayon, fill = True, facecolor=couleur[données.index(élément)], edgecolor="white") # dessine une série de ronds d'aire 1600 
        ax.add_artist(cercle) 
        # On s'arrête entre chaque tracé de cercle
        plt.pause(0.01)

        # --- CODE DES COLLISIONS ENTRE BALLES --- #
        for liste in données:
            for autre_liste in données:
                contamination:str="green" 
                dx = autre_liste[0] - liste[0]
                dy = autre_liste[1] - liste[1] 
                dist = np.sqrt(dx**2 + dy**2)
                if couleur[données.index(liste)]=="red" or couleur[données.index(autre_liste)]=="red":
                    contamination="red"
            if dist <= 2.1*rayon and dist>0:
                nx, ny = dx/dist, dy/dist # vecteur normal unitaire 
                tx , ty = -ny, nx # vecteur tangente unitaire
                vn1 = (liste[2]*nx + liste[3]*ny) # projection 1 normale
                vt1 = (liste[2]*tx + liste[3]*ty) # projection 1 tangentielle 
                vn2 = (autre_liste[2]*nx + autre_liste[3]*ny) # projection 2 normale
                vt2 = (autre_liste[2]*tx + autre_liste[3]*ty) # projection 2 tangentielle
                vnP1, vnP2 = vn2, vn1 # les vecteurs normaux sont échangés comme un choc 1D
                vtP1, vtP2 = vt1, vt2 # vecteurs tan inchangés
                liste[2] = vnP1*nx + vt1*tx # v = vn + vt
                liste[3] = vnP1*ny + vt1*ty
                autre_liste[2] = vnP2*nx + vt2*tx
                autre_liste[3] = vnP2*ny + vt2*ty

                # --- CODE DE LA CONTAMINATION D'UNE BALLE --- #
                if contamination=="red":
                    if couleur[données.index(liste)]=="green":
                        couleur[données.index(liste)]="red"
                    else:
                        couleur[données.index(autre_liste)]="red"
# On montre le tout
plt.show()